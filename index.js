(function () {
    'use strict';
    const format = require('util').format;
    const logger = require('logger')();

    module.exports.HTTP200 = module.exports.ok = function (res) {
        return function (data) {
            let result = res
                .status(200)
                .json(data)
                .end();
            return result;
        };
    };

    module.exports.HTTP302 = module.exports.redirect = function (res) {
        return function(url) {
            res.setHeader('Location', url);
            res.setHeader('Content-Length', '0');
            let result = res
                .status(302)
                .end();
            return result;
        };
    };

    module.exports.HTTP400 = module.exports.badRequest = function (res) {
        return function (...args) {
            const msg = format('Bad request: %s', format(...args));
            logger.warn(msg);
            return res.status(400).send({
                success: false,
                message: msg
            }).end();
        };
    };

    module.exports.HTTP401 = module.exports.unauthorized = function (res) {
        return function (...args) {
            const msg = format('Unauthorized: %s', format(...args));
            logger.warn(msg);
            return res.status(401).send({
                success: false,
                message: msg
            }).end();
        };
    };

    module.exports.HTTP403 = module.exports.forbidden = function (res) {
        return function (...args) {
            const msg = format('Forbidden: %s', format(...args));
            logger.warn(msg);
            return res.status(403).send({
                success: false,
                message: msg
            }).end();
        };
    };

    module.exports.HTTP404 = module.exports.notFound = function (res) {
        return function (...args) {
            const msg = format('Not found: %s', format(...args));
            logger.warn(msg);
            return res.status(404).send({
                success: false,
                message: msg
            }).end();
        };
    };

    module.exports.HTTP500 = module.exports.internalServerError = function (res) {
        return function (...args) {
            const msg = format('Internal server error: %s', format(...args));
            logger.error(msg);
            return res.status(500).send({
                success: false,
                message: msg
            }).end();
        }
    };

}());

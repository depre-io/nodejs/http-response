describe('http-response', function () {
    const chai = require('chai')
    const expect = chai.expect
    const sinonChai = require('sinon-chai')
    const mockRes = require('sinon-express-mock').mockRes
    chai.use(sinonChai)
    let httpResponse = require('../index')
    let res

    // add a test hook executed before each individual test
    beforeEach(function () {
        res = mockRes()
    })

    describe('should implement the HTTP200 interface', function () {


        it('function', function () {
            // add an assertion
            expect(httpResponse.HTTP200).to.be.a('function')


        })
        it('set the response code to 200', function () {
            // Call the method
            httpResponse.HTTP200(res)({ "foo": "bar" })
            expect(res.status).to.be.calledWith(200)
        })
        it('apply the passed json object to the response', function () {
            // Call the method
            httpResponse.HTTP200(res)({ "foo": "bar" })
            expect(res.json).to.be.calledWith({ "foo": "bar" })
        })
    })

    // ...some more tests
    describe('should implement the HTTP302 interface', function () {

        it('function', function () {
            expect(httpResponse.HTTP302).to.be.a('function')

        })
        
        it('should be able to redirect to the given `url` with optional response `status` defaulting to 302.', function (done) {
            var response = new MockExpressResponse(url)
            response.redirect(url)
            expect(response.get('Location')).to.be.equal()
            done()
        })
    })

    describe('should implement the HTTP400 interface', function () {

        it('function', function () {
            expect(httpResponse.HTTP400).to.be.a('function')

        })
        it('set the response code to 400', function () {
            httpResponse.HTTP400(res)({ "Bad request": "%s" })
            expect(res.status).to.be.calledWith(400)
        })
    })

    describe('should implement the HTTP401 interface', function () {


        it('function', function () {
            expect(httpResponse.HTTP401).to.be.a('function')

        })
        it('set the response code to 401', function () {
            httpResponse.HTTP401(res)({ "Unauthorized": "%s" })
            expect(res.status).to.be.calledWith(401)
        })
    })

    describe('should implement the HTTP403 interface', function () {


        it('function', function () {
            expect(httpResponse.HTTP403).to.be.a('function')

        })
        it('set the response code to 403', function () {
            httpResponse.HTTP403(res)({ "Forbidden": "%s" })
            expect(res.status).to.be.calledWith(403)
        })

        describe('should implement the HTTP404 interface', function () {


            it('function', function () {
                expect(httpResponse.HTTP404).to.be.a('function')

            })
            it('set the response code to 404', function () {
                httpResponse.HTTP404(res)({ "Not found": "%s" })
                expect(res.status).to.be.calledWith(404)
            })
        })


        describe('should implement the HTTP500 interface', function () {


            it('function', function () {
                expect(httpResponse.HTTP500).to.be.a('function')

            })
            it('set the response code to 500', function () {
                httpResponse.HTTP500(res)({ "Internal server error": "%s" })
                expect(res.status).to.be.calledWith(500)
            })
        })

    })
})

# Installation
this is a [node.js](https://nodejs.org/en/) module. To be able to use this module, install the latest version of [Node.js](https://nodejs.org/en/download/)

If this is a new project, create a package.json file first.
To do this you use the npm init command in the command prompt and follow the instructions on screen.
Now to install http-response you use the npm install command
```
npm install http-response
```

# What is http-response?
Http-response is a Node.js module which makes it easier to generate pages for when a web process returns an error.

# Features
* making it easier to return errors
* makes it possible to use both httpResponse.HTTP400 and httpResponse.badRequest
## Examples
First an example which uses httpResponse.badRequest.
```
app.get("/", (req, res, next) => {
 return httpResponse.badRequest(res)('NotFound')
```
The second example makes use of httpResponse.HTTP.
```
app.get("/", (req, res, next) => {
 return httpResponse.HTTP404(res)(
```
